// JS closure

/*  JS closure is a function that is defined in another function.
 *  The inner function can access all the local variables of the outer function.
 *  If the inner function is returned, then the lcoal variable of the outer function
 *  is still alive after the outer function is executed.
 *  Disadvantage: Memory leak
*/
function outer(){
  var m = 10;
  return function(x){
    return x * m;
  }
}

var inner = outer();
console.log(inner(2));

function getCounter(){
  var count = 0;
  return {
    getCounter: function(){
      return count;
    },
    setCount: function(c){
      count = c;
    },
    increase: function(){
      count++;
    }
  };
}

var myCounter = getCounter();
console.log(myCounter.getCounter());

myCounter.setCount(1);
console.log(myCounter.getCounter());

var myCounter1 = (function(){
  var count = 0;
  return {
    getCounter: function(){
      return count;
    },
    setCount: function(c){
      count = c;
    },
    increase: function(){
      count++;
    }
  };
})();

//this
function MyClass(){
  this.x = 100;
  var y = "abc";
  var getX = function(){
    console.log(this.x);
    console.log(y);
  }
  // getX();
  this.getY = function(){
    console.log(this.x);
    console.log(y);
  }
}

var myClass = new MyClass();
console.log(myClass.x);
myClass.getY();

//singleton
var MySingle = (function(){
  var instance;
  return {
    getInstance: function(){
      if(!instance){
        instance = new Object();
      }
      return instance;
    }
  }
})()

var ms1 = MySingle.getInstance();
var ms2 = MySingle.getInstance();
console.log(ms1 === ms2);

var myTimeout = setTimeout(function(){
  console.log("after 2 second");
}, 2000);
// clearTimeout(myTimeout);

console.log("after setTimeout");

setInterval(function(){
  console.log("every 2 second");
}, 2000)
