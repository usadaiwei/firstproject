//JS object property attributes and object status

//A property has 3 attributes: enumerable, writable, configurable
var alice = {
    "name": "Alice"
};
alice.age = 30; //all 3 attributes are true
console.log(Object.keys(alice));
alice.age = 40;
console.log(alice.age);

Object.defineProperty(alice,"salary", {
    value: 1000 // all 3 attributes are false
    //enumerable: true
})
console.log(Object.keys(alice));
console.log(Object.getOwnPropertyNames(alice));// get enumerable: false property
alice.salary = 2000;
console.log(alice.salary);

Object.defineProperty(alice, "address", {
    value: "Princeton",
    enumerable: true,
    //configurable: true
})
console.log(Object.keys(alice));

delete alice.address;  //configurable: false couldn't be delete
delete alice.age
console.log(Object.keys(alice));

Object.defineProperty(alice, "color", {
    configurable: true
});

alice.color = 100;
console.log(alice.color);

Object.defineProperty(alice, "color", {
    writable:true
});
alice.color = 100;
console.log(alice.color);

//Three status of an object: extensible, sealed, frozen
var obj = {
    x: "abc"
};
console.log('extensible: ', Object.isExtensible(obj));
Object.preventExtensions(obj);
console.log('extensible: ', Object.isExtensible(obj));
obj.y = 100; //extensible: false couldn't add property
console.log(obj.y);

console.log('sealed', Object.isSealed(obj));
Object.seal(obj);
console.log('sealed', Object.isSealed(obj));
delete obj.x; //when sealed is true the object couldn't be delete and add, but it can be update
console.log(obj);

console.log('frozen', Object.isFrozen(obj));
Object.freeze(obj);
console.log('frozen', Object.isFrozen(obj));
obj.x = "def";
delete obj.x; //when frozen is true the object couldn't be delete, add, and update
console.log(obj);

//Note: all primitive types are frozen,
var s = "abc";
console.log('frozen', Object.isFrozen(s));
