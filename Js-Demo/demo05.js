// JS: this
var assert = require('assert');
function Vehicle() {
    this.price = 1000; //global.price = 1000,
    age = 33;  //global,
    var name = 222;  //local
}
var v = new Vehicle(); //generate a scope
console.log(v);
console.log(v.price);
console.log(global.price);
console.log('after new', global.name);
console.log(v.age);

var u = Vehicle();
console.log(u);
console.log(global.age);
console.log('after function invoke', global.price);

// safe constructor.
function Person(name) {
    if (!(this instanceof Person)) {  //check if global has Person
        return new Person(name);
    }
    this.name = name;
}
console.log(Person('bob'));

var bob = {
    name: "Bob",
    hello: function(a, b) {
        return "Hello " + this.name;
    }
};
console.log(bob.hello());
var myHello = bob.hello;
console.log(myHello(1, 2));

function f2(){
  'use strict';
  return this;
}

assert.ok(f2() === undefined);

//Function: apply, call, bind
console.log(myHello.apply(bob,[1,2])); //array
console.log(myHello.call(bob,1,2));   //,
console.log(myHello.bind(bob)(1,2)); //()

(function(){
  var order = {
    price: 100,
    calculate: function(){
      var that = this;
      return{
        name: "table",
        getTotalPrice: function(qty){
          return qty * that.price;
        }
      };
    },
    calculate2: function(){
      return{
        name: "table",
        getTotalPrice: function(qty){
          return qty * this.price;
        }
      };
    }
  };

  var calc = order.calculate();
  console.log(calc);
  console.log(calc.name);
  console.log(calc.getTotalPrice(1));

  var calc2 = order.calculate2();
  console.log(calc2.getTotalPrice.apply(order,[1]));
  console.log(calc2.getTotalPrice.call(order,1));
  console.log(calc2.getTotalPrice.bind(order)(1));
})()

function checkout(price, qty, member){
  var result = price * qty;
  switch(member){
    case "VIP":
      result *= 0.7;
      break;
    case "Premium":
      result *= 0.6;
      break;
    default:
      break;
  }
  return result;
}
console.log(checkout(100, 5, "VIP"));

function checkout2(price, qty, type){
  this[type] = this[type] || 1;
  return price * qty * this[type];
}

var member = {
  "VIP": 0.7,
  "Premium": 0.8,
  "Super VIP": 0.5
}

console.log(checkout2.call(member, 100, 5, "Premium"));
